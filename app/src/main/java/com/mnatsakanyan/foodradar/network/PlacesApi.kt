package com.chama.foodradar.network

import com.chama.foodradar.model.PlacesResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to get result
 */
interface PlacesApi {
    @GET("/maps/api/place/nearbysearch/json")
    fun getPlaces(
        @Query("location") location: String,
        @Query("type") type: String,
        @Query("rankby") rankBy: String,
        @Query("key") key: String
    ): Observable<PlacesResult>
}
