package com.chama.foodradar.base

import androidx.lifecycle.Lifecycle

/**
 * Base view any view must implement.
 */
interface BaseView {
    fun getLifecycle(): Lifecycle
}
