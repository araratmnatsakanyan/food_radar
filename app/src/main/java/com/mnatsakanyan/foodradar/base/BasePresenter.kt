package com.chama.foodradar.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.chama.foodradar.RadarApplication
import com.chama.foodradar.ui.places.PlacesPresenter

/**
 * Base presenter any presenter of the application must extend. It provides initial injections and
 * required methods.
 */
abstract class BasePresenter<out V : BaseView>(
    protected val view: V
) : LifecycleObserver, LifecycleOwner {

    init {
        inject()
    }

    override fun getLifecycle() = view.getLifecycle()

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun destroy() {
        view.getLifecycle().removeObserver(this)
    }

    fun initialize() {
        view.getLifecycle().addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun pause() {
        // To be overridden
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun resume() {
        // To be overridden
    }

    private fun inject() {
        when (this) {
            is PlacesPresenter -> RadarApplication.injector.inject(this)
        }
    }
}
