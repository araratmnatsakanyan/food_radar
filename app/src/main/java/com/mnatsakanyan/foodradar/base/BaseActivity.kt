package com.chama.foodradar.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Activity all Activity classes of rosso must extend. It provides required methods and presenter
 * instantiation and calls.
 */
abstract class BaseActivity<P : BasePresenter<BaseView>> : AppCompatActivity(), BaseView {
    protected lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = instantiatePresenter()
    }

    protected abstract fun instantiatePresenter(): P
}
