package com.chama.foodradar.model

import com.google.gson.annotations.SerializedName

data class PlacesResult(
    @SerializedName("results") val results: List<Place>,
    @SerializedName("status") val status: String
)
