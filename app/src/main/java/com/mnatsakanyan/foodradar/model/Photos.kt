package com.chama.foodradar.model

import com.google.gson.annotations.SerializedName

data class Photos(
    @SerializedName("photo_reference") val photo_reference: String?
)
