package com.chama.foodradar.model

import com.google.gson.annotations.SerializedName

data class Place(
    @SerializedName("geometry") val geometry: Geometry?,
    @SerializedName("id") val id: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("opening_hours") val opening_hours: OpeningHours?,
    @SerializedName("photos") val photos: List<Photos>?,
    @SerializedName("rating") val rating: Double?,
    @SerializedName("types") val types: List<String>?
)
