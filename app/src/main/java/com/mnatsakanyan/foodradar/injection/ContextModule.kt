package com.chama.foodradar.injection

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Module which provides all required dependencies about context
 */
@Module
class ContextModule(private val context: Context) {
    @Provides
    @Reusable
    internal fun provideContext(): Context {
        return context
    }

    @Provides
    @Reusable
    internal fun provideApplication(): Application {
        return context.applicationContext as Application
    }
}
