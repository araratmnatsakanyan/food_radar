package com.chama.foodradar.injection

import android.content.Context
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Module which provides all required dependencies about location
 */
@Module
object LocationModule {
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideFusedLocationRequest(): LocationRequest {
        return LocationRequest.create().apply {
            interval = 10000L
            fastestInterval = 10000L / 2
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }
}
