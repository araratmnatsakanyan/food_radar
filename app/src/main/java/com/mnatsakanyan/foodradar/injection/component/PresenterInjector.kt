package com.chama.foodradar.injection.component

import com.chama.foodradar.injection.ContextModule
import com.chama.foodradar.injection.LocationModule
import com.chama.foodradar.injection.NetworkModule
import com.chama.foodradar.ui.places.PlacesPresenter
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() method for presenter.
 */
@Singleton
@Component(modules = [(ContextModule::class), (LocationModule::class), (NetworkModule::class)])
interface PresenterInjector {
    fun inject(placesPresenter: PlacesPresenter)

    @Component.Builder
    interface Builder {
        fun build(): PresenterInjector

        fun contextModule(contextModule: ContextModule): Builder

        fun locationModule(locationModule: LocationModule): Builder

        fun networkModule(networkModule: NetworkModule): Builder
    }
}
