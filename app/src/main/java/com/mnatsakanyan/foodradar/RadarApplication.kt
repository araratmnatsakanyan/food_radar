package com.chama.foodradar

import android.app.Application
import com.chama.foodradar.injection.ContextModule
import com.chama.foodradar.injection.LocationModule
import com.chama.foodradar.injection.NetworkModule
import com.chama.foodradar.injection.component.DaggerPresenterInjector
import com.chama.foodradar.injection.component.PresenterInjector

class RadarApplication : Application() {
    companion object {
        @JvmStatic
        lateinit var injector: PresenterInjector
    }

    override fun onCreate() {
        super.onCreate()

        injector = DaggerPresenterInjector
            .builder()
            .contextModule(ContextModule(applicationContext))
            .networkModule(NetworkModule)
            .locationModule(LocationModule)
            .build()
    }
}
