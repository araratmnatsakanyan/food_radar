package com.chama.foodradar.ui.places

import com.chama.foodradar.base.BaseView
import com.chama.foodradar.model.Location
import com.chama.foodradar.model.Place

/**
 * Interface providing required methods for view displaying places
 */
interface PlacesView : BaseView {
    fun isPermissionsGranted(): Boolean

    fun requestLocationPermissions()

    fun setEmptyListOverlayVisibility(visibility: Boolean)

    fun setLoadingVisibility(visibility: Boolean)

    fun setLocationPermissionOverlayVisibility(visibility: Boolean)

    fun shouldShowRequestPermissionRationale(): Boolean

    fun updatePlaces(places: List<Place>, currentLocation: Location?)
}
