package com.chama.foodradar.ui.places

import android.Manifest
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.chama.foodradar.R
import com.chama.foodradar.base.BaseActivity
import com.chama.foodradar.databinding.ActivityPlacesBinding
import com.chama.foodradar.model.Location
import com.chama.foodradar.model.Place
import com.chama.foodradar.utils.isLocationPermissionsGranted
import com.chama.foodradar.utils.shouldShowLocationRequestPermissionRationale
import kotlinx.android.synthetic.main.activity_places.*

/**
 * Main activity of the app and shows the places
 */
class PlacesActivity : BaseActivity<PlacesPresenter>(), PlacesView {
    private lateinit var binding: ActivityPlacesBinding

    private val postsAdapter = PlaceAdapter(this)

    override fun instantiatePresenter() = PlacesPresenter(this)

    companion object {
        private const val REQUEST_CODE_LOCATION = 100
    }

    override fun isPermissionsGranted() = isLocationPermissionsGranted()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_places)
        binding.adapter = postsAdapter
        binding.layoutManager = LinearLayoutManager(this)
        binding.presenter = presenter

        presenter.initialize()
        presenter.loadPlaces()

        swipeRefreshLayout.setOnRefreshListener { presenter.onRefresh() }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_CODE_LOCATION -> {
                presenter.onLocationRequest()
            }
        }
    }

    override fun requestLocationPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            REQUEST_CODE_LOCATION
        )
    }

    override fun setEmptyListOverlayVisibility(visibility: Boolean) {
        binding.emptyOverlayVisibility = visibility
    }

    override fun setLoadingVisibility(visibility: Boolean) {
        if (!swipeRefreshLayout.isRefreshing) {
            binding.progressVisibility = visibility
        }
    }

    override fun setLocationPermissionOverlayVisibility(visibility: Boolean) {
        binding.locationPermissionVisibility = visibility
    }

    override fun shouldShowRequestPermissionRationale() =
        shouldShowLocationRequestPermissionRationale()

    override fun updatePlaces(places: List<Place>, currentLocation: Location?) {
        swipeRefreshLayout.isRefreshing = false
        postsAdapter.updatePlaces(places, currentLocation)
    }
}
