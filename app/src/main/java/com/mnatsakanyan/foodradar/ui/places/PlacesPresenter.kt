package com.chama.foodradar.ui.places

import androidx.lifecycle.LifecycleOwner
import com.chama.foodradar.BuildConfig
import com.chama.foodradar.base.BasePresenter
import com.chama.foodradar.model.Location
import com.chama.foodradar.model.Place
import com.chama.foodradar.network.PlacesApi
import com.chama.foodradar.utils.simplifiedCoordinated
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PlacesPresenter(view: PlacesView) : BasePresenter<PlacesView>(view), LifecycleOwner {
    private var lastKnownLocation: Location? = null

    @Inject
    lateinit var fusedLocationClient: FusedLocationProviderClient

    @Inject
    lateinit var locationRequest: LocationRequest

    @Inject
    lateinit var placesApi: PlacesApi

    private var subscription: Disposable? = null

    private var type: String = "restaurant"

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            for (location in locationResult.locations) {
                updateLastKnownLocation(Location(location.latitude, location.longitude))
            }
        }
    }

    override fun destroy() {
        super.destroy()

        subscription?.dispose()
    }

    override fun pause() {
        super.pause()

        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun resume() {
        super.resume()

        startLocationUpdates()
    }

    fun loadPlaces(type: String? = null) {
        type?.let { this.type = type }
        requestPlaces()
    }

    fun onLocationRequest() {
        invokeLocationAction()
    }

    fun onRefresh() {
        loadPlaces()
    }

    fun onTypeClick(type: String) {
        loadPlaces(type)
    }

    fun requestLocationPermissions() {
        view.requestLocationPermissions()
    }

    fun updateLastKnownLocation(location: Location) {
        val initialLastKnownLocation = lastKnownLocation
        lastKnownLocation = location
        initialLastKnownLocation ?: requestPlaces()
    }

    private fun invokeLocationAction() {
        when {
            view.isPermissionsGranted() -> {
                view.setLocationPermissionOverlayVisibility(false)
                startLocationUpdates()
            }
            view.shouldShowRequestPermissionRationale() -> {
                view.setLoadingVisibility(false)
                view.setLocationPermissionOverlayVisibility(true)
            }
            else -> requestLocationPermissions()
        }
    }

    private fun requestFailed() {
        view.setEmptyListOverlayVisibility(true)
        view.updatePlaces(listOf(), null)
    }

    private fun requestPlaces() {
        view.setLoadingVisibility(true)
        lastKnownLocation?.simplifiedCoordinated()?.let {
            subscription = placesApi.getPlaces(
                it,
                type,
                "distance",
                BuildConfig.PLACES_API_KEY
            ).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnTerminate { view.setLoadingVisibility(false) }
                .subscribe(
                    { response -> updatePlaces(response.results) },
                    { requestFailed() }
                )
        } ?: invokeLocationAction()
    }

    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private fun updatePlaces(places: List<Place>) {
        view.setEmptyListOverlayVisibility(places.isEmpty())
        view.updatePlaces(places, lastKnownLocation)
    }
}
