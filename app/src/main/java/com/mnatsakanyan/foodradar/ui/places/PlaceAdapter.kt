package com.chama.foodradar.ui.places

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.chama.foodradar.R
import com.chama.foodradar.databinding.ItemPlaceBinding
import com.chama.foodradar.model.Location
import com.chama.foodradar.model.Place
import com.chama.foodradar.utils.androidLocation

class PlaceAdapter(
    private val context: Context
) : RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder>() {
    private var places: List<Place> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private var currentLocation: Location? = null

    override fun getItemCount() = places.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemPlaceBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_place,
            parent,
            false
        )

        return PlaceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.bind(places[position], currentLocation)
    }

    fun updatePlaces(places: List<Place>, currentLocation: Location?) {
        this.places = places
        this.currentLocation = currentLocation
        notifyDataSetChanged()
    }

    class PlaceViewHolder(private val binding: ItemPlaceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(place: Place, currentLocation: Location?) {
            binding.place = place
            binding.currentLocation = currentLocation?.androidLocation()
            binding.placeLocation = place.geometry?.location?.androidLocation()
            binding.executePendingBindings()
        }
    }
}
