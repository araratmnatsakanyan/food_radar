package com.chama.foodradar.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.chama.foodradar.model.Location

/**
 * Util class to give us info about location status, if user gave the permission or not
 */
fun Activity.isLocationPermissionsGranted() =
    ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED

fun Activity.shouldShowLocationRequestPermissionRationale() =
    ActivityCompat.shouldShowRequestPermissionRationale(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) && ActivityCompat.shouldShowRequestPermissionRationale(
        this,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

fun Location.simplifiedCoordinated() = "$lat,$lng"

fun Location.androidLocation(): android.location.Location {
    val location: android.location.Location = android.location.Location(lat.toString())
    lat?.run { location.latitude = this }
    lng?.run { location.longitude = this }

    return location
}
