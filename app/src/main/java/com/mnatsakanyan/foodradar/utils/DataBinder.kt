// Safe here as method are used by Data binding
@file:Suppress("unused")

package com.chama.foodradar.utils

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.chama.foodradar.ui.places.PlaceAdapter

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: PlaceAdapter) {
    view.adapter = adapter
}

@BindingAdapter("layoutManager")
fun setAdapter(view: RecyclerView, layoutManager: RecyclerView.LayoutManager) {
    view.layoutManager = layoutManager
}
