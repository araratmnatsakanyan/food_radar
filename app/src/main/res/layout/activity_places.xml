<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:bind="http://schemas.android.com/apk/res-auto">

    <data>

        <import type="android.view.View" />

        <variable
            name="adapter"
            type="com.chama.foodradar.ui.places.PlaceAdapter" />

        <variable
            name="layoutManager"
            type="androidx.recyclerview.widget.LinearLayoutManager" />

        <variable
            name="outlineProvider"
            type="android.view.ViewOutlineProvider" />

        <variable
            name="presenter"
            type="com.chama.foodradar.ui.places.PlacesPresenter" />

        <variable
            name="progressVisibility"
            type="Boolean" />

        <variable
            name="emptyOverlayVisibility"
            type="Boolean" />

        <variable
            name="locationPermissionVisibility"
            type="Boolean" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".ui.places.PlacesActivity">

        <com.google.android.material.appbar.AppBarLayout
            android:layout_width="match_parent"
            android:layout_height="@dimen/toolbar_height"
            android:background="@color/colorWhiteBackground"
            app:elevation="0dp">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/toolbar"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:paddingTop="@dimen/standard_padding"
                android:paddingBottom="@dimen/standard_padding"
                app:contentInsetEnd="0dp"
                app:contentInsetLeft="0dp"
                app:contentInsetRight="0dp"
                app:contentInsetStart="0dp"
                app:layout_scrollFlags="scroll">

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent">

                    <androidx.appcompat.widget.AppCompatImageView
                        android:id="@+id/restaurantImageView"
                        style="@style/CategoryButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:backgroundTint="@color/colorBlueBackground"
                        android:onClick='@{() -> presenter.onTypeClick("restaurant")}'
                        android:outlineProvider="@{outlineProvider}"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintEnd_toStartOf="@+id/cafeImageView"
                        app:layout_constraintHorizontal_chainStyle="spread"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent"
                        app:srcCompat="@drawable/ic_restaurant_white_24dp" />

                    <TextView
                        style="@style/TextAppearance.AppCompat.Caption"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_margin="@dimen/standard_half_padding"
                        android:text="@string/category_restaurants"
                        app:layout_constraintEnd_toEndOf="@id/restaurantImageView"
                        app:layout_constraintStart_toStartOf="@id/restaurantImageView"
                        app:layout_constraintTop_toBottomOf="@+id/restaurantImageView" />

                    <androidx.appcompat.widget.AppCompatImageView
                        android:id="@+id/cafeImageView"
                        style="@style/CategoryButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:backgroundTint="@color/colorGreenBackground"
                        android:onClick='@{() -> presenter.onTypeClick("cafe")}'
                        android:outlineProvider="@{outlineProvider}"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintEnd_toStartOf="@+id/barImageView"
                        app:layout_constraintStart_toEndOf="@+id/restaurantImageView"
                        app:layout_constraintTop_toTopOf="parent"
                        app:srcCompat="@drawable/ic_local_cafe_white_24dp" />

                    <TextView
                        style="@style/TextAppearance.AppCompat.Caption"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_margin="@dimen/standard_half_padding"
                        android:text="@string/category_cafes"
                        app:layout_constraintEnd_toEndOf="@id/cafeImageView"
                        app:layout_constraintStart_toStartOf="@id/cafeImageView"
                        app:layout_constraintTop_toBottomOf="@+id/cafeImageView" />

                    <androidx.appcompat.widget.AppCompatImageView
                        android:id="@+id/barImageView"
                        style="@style/CategoryButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:backgroundTint="@color/colorOrangeBackground"
                        android:onClick='@{() -> presenter.onTypeClick("bar")}'
                        android:outlineProvider="@{outlineProvider}"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toEndOf="@+id/cafeImageView"
                        app:layout_constraintTop_toTopOf="parent"
                        app:srcCompat="@drawable/ic_local_bar_white_24dp" />

                    <TextView
                        style="@style/TextAppearance.AppCompat.Caption"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_margin="@dimen/standard_half_padding"
                        android:text="@string/category_bars"
                        app:layout_constraintEnd_toEndOf="@id/barImageView"
                        app:layout_constraintStart_toStartOf="@id/barImageView"
                        app:layout_constraintTop_toBottomOf="@+id/barImageView" />

                </androidx.constraintlayout.widget.ConstraintLayout>

            </androidx.appcompat.widget.Toolbar>

        </com.google.android.material.appbar.AppBarLayout>

        <androidx.swiperefreshlayout.widget.SwipeRefreshLayout
            android:id="@+id/swipeRefreshLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorWhiteBackground"
            app:layout_behavior="@string/appbar_scrolling_view_behavior">

            <androidx.recyclerview.widget.RecyclerView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:adapter="@{adapter}"
                app:layoutManager="@{layoutManager}" />

        </androidx.swiperefreshlayout.widget.SwipeRefreshLayout>

        <FrameLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="@color/colorTransparentGreyBackground"
            android:elevation="4dp"
            android:visibility="@{progressVisibility ? View.VISIBLE : View.GONE}">

            <ProgressBar
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center" />

        </FrameLayout>

        <include
            android:id="@+id/emptyListOverlay"
            layout="@layout/item_empty_list"
            android:visibility="@{emptyOverlayVisibility ? View.VISIBLE : View.GONE}" />

        <include
            android:id="@+id/locationPermissionOverlay"
            layout="@layout/item_grant_location"
            android:visibility="@{locationPermissionVisibility ? View.VISIBLE : View.GONE}"
            bind:presenter="@{presenter}" />

    </androidx.coordinatorlayout.widget.CoordinatorLayout>

</layout>
